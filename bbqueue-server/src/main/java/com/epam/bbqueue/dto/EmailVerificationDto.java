package com.epam.bbqueue.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class EmailVerificationDto {
    @NotNull
    private String email;
    @NotNull
    private String code;
}
