package com.epam.bbqueue.exception;

public class EmptyVerificationTokenException extends RuntimeException {
    public EmptyVerificationTokenException(String message) {
        super(message);
    }
}
