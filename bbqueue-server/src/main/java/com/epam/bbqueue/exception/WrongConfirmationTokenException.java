package com.epam.bbqueue.exception;

public class WrongConfirmationTokenException extends RuntimeException {
    public WrongConfirmationTokenException(String message) {
        super(message);
    }
}
