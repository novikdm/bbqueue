package com.epam.bbqueue.exception;

public class AlreadyConfirmedVerificationTokenException extends RuntimeException {
    public AlreadyConfirmedVerificationTokenException(String message) {
        super(message);
    }
}
