package com.epam.bbqueue.exception;

public class DoctorSpecialityNotFoundException extends RuntimeException{

    public DoctorSpecialityNotFoundException(String message) {
        super(message);
    }

}
