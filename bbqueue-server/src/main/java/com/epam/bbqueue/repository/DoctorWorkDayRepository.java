package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.DoctorWorkDay;
import com.epam.bbqueue.entity.enums.Day;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DoctorWorkDayRepository extends JpaRepository<DoctorWorkDay, Long> {

    List<DoctorWorkDay> findAllByDoctorId(Long id);

    Optional<DoctorWorkDay> findByDay(Day day);

    Optional<DoctorWorkDay> findByDayAndDoctor_Id(Day day, Long id);
}
