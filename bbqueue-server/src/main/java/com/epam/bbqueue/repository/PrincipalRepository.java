package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.Principal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrincipalRepository extends JpaRepository<Principal, Long> {

    Optional<Principal> findByEmail(String email);

    boolean existsByEmail(String email);

    void findAllByEmailAndPassword(String email, String password);
}
