package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.VisitResult;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VisitResultRepository extends JpaRepository<VisitResult, Long> {

    List<VisitResult> findByDescription(String description);

}
