package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.Role;
import com.epam.bbqueue.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName name);

    Optional<Role> findByName(String name);
}
