package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.TimeSlot;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long> {

    @EntityGraph(value = "timeSlot-visitResult-graph", type = EntityGraph.EntityGraphType.LOAD)
    List<TimeSlot> findAllByCustomer_IdAndVisitDateLessThan(Long idUser, Date date);
    @EntityGraph(value = "timeSlot-visitResult-graph", type = EntityGraph.EntityGraphType.LOAD)
    List<TimeSlot> findAllByCustomer_IdAndVisitDateGreaterThanEqual(Long id, Date date);

    List<TimeSlot> findAllByVisitDateAndService_Doctor_Id(Date date, Long doctorId);
}
