package com.epam.bbqueue.repository;

import com.epam.bbqueue.entity.Region;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {

    @EntityGraph(attributePaths = {"country"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<Region> findByNameContaining(String regionName);

    @EntityGraph(attributePaths = {"country"}, type = EntityGraph.EntityGraphType.LOAD)
    @Override
    List<Region> findAll();

}
