package com.epam.bbqueue.entity.enums;

public enum LogLevels {
    TRACE, INFO, DEBUG, WARN, ERROR;
}
