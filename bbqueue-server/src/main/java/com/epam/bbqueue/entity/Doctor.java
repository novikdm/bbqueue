package com.epam.bbqueue.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NamedEntityGraph(
        name = "doctor-doctorWokDays-graph",
        attributeNodes = {
                @NamedAttributeNode("doctorWorkDays")
        }
)
@NamedEntityGraph(
        name = "doctor-doctorService-graph",
        attributeNodes = {
                @NamedAttributeNode("services")
        }
)
@NamedEntityGraph(
        name = "doctor-user-graph",
        attributeNodes = {
                @NamedAttributeNode("customers")
        }
)
@NamedEntityGraph(
        name = "doctor-search-form-graph",
        attributeNodes = {
                @NamedAttributeNode("speciality"),
                @NamedAttributeNode(value = "hospital"),
        },
        subclassSubgraphs = {
                @NamedSubgraph(name = "hospital-subgraphs", type = Hospital.class, attributeNodes = { @NamedAttributeNode(value = "region") }),
        }
)
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer cabinetNumber;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospital_id")
    private Hospital hospital;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "speciality_id", nullable = false)
    private DoctorSpeciality speciality;

    @OneToMany(mappedBy = "doctor")
    @ToString.Exclude
    private Set<DoctorWorkDay> doctorWorkDays;

    @OneToMany(mappedBy = "doctor")
    @ToString.Exclude
    @JsonIgnore
    private Set<DoctorService> services;

    @OneToMany
    @ToString.Exclude
    @JoinTable(name = "client_family_doctor", joinColumns = @JoinColumn(name = "doctor_id"), inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private Set<User> customers;
}




