package com.epam.bbqueue.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
@NamedEntityGraph(
        name = "hospital-region-graph",
        attributeNodes = {
                @NamedAttributeNode(value = "region", subgraph = "region-subgraph"),
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "region-subgraph",
                        attributeNodes = {
                                @NamedAttributeNode("country")
                        }
                )

        }
)
@Entity
@Data
public class Hospital {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id", nullable = false)
    private Region region;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "head_doctor_id")
    private Doctor headDoctor;
}
