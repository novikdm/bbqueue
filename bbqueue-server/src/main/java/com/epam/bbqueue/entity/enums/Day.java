package com.epam.bbqueue.entity.enums;

import java.util.Arrays;
import java.util.Optional;

public enum Day {
    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(0);

    int dayOrdinal;

    Day(int ordinal) {
        this.dayOrdinal = ordinal;
    }

    public static Optional<Day> getDayByOrdinal(int ordinalNumber) {
        return Arrays.stream(Day.values())
                .filter(day -> day.dayOrdinal == ordinalNumber)
                .findFirst();
    }
}
