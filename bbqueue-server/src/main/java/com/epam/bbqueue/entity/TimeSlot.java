package com.epam.bbqueue.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@NamedEntityGraph(
        name = "timeSlot-visitResult-graph",
        attributeNodes = {
                @NamedAttributeNode("visitResult")
        }
)
public class TimeSlot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Date should not be null")
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date visitDate;

    @NotNull(message = "TimeSlot number should not be null")
    @Min(value = 1, message = "TimeSlot number must be greater than 0")
    @Column(nullable = false)
    private Integer timeSlotNumber;

    @NotNull(message = "User should not be null")
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private User customer;

    @NotNull(message = "Service should not be null")
    @ManyToOne
    @JoinColumn(name = "service_id", nullable = false)
    private DoctorService service;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "visit_result_id", nullable = false)
    private VisitResult visitResult;
}
