package com.epam.bbqueue.entity;

import com.epam.bbqueue.entity.enums.Day;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NamedEntityGraph(
        name = "doctorWorkDay-doctor-graph",
        attributeNodes = {
                @NamedAttributeNode("doctor")
        }
)
public class DoctorWorkDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Day day;

    @Column(nullable = false)
    @Temporal(TemporalType.TIME)
    private Date workStartTime;

    @Column(nullable = false)
    @Temporal(TemporalType.TIME)
    private Date workEndTime;

    @Temporal(TemporalType.TIME)
    private Date lunchStartTime;

    @Temporal(TemporalType.TIME)
    private Date lunchEndTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id", nullable = false)
    private Doctor doctor;
}
