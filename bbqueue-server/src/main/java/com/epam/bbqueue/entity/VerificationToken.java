package com.epam.bbqueue.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VerificationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String token;
    @Column(nullable = false)
    private boolean isConfirmed;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date issuedDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredDateTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date confirmedDateTime;
    @OneToOne(fetch = FetchType.LAZY)
    private Principal principal;
}
