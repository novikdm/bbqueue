package com.epam.bbqueue.entity.enums;

public enum RoleName {
    USER, DOCTOR, HEAD_DOCTOR, ADMIN
}
