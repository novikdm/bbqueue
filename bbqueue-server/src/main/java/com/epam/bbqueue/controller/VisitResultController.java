package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.service.interfaces.VisitResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/visits")
public class VisitResultController {

    private final VisitResultService timeSlotService;

    @Autowired
    public VisitResultController(VisitResultService timeSlotService) {
        this.timeSlotService = timeSlotService;
    }

    @GetMapping("/history/{idUser}")
    public List<TimeSlot> getAllHistoryOfVisitsForUser(@PathVariable Long idUser){
        return timeSlotService.getAllHistoryOfVisitsForUser(idUser, new Date());
    }

    @GetMapping("/active/{idUser}")
    public List<TimeSlot> getAllActiveQueuePlace(@PathVariable Long idUser){
        return timeSlotService.getAllActiveQueuePlace(idUser, new Date());
    }

}
