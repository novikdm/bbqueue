package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Referral;
import com.epam.bbqueue.service.interfaces.ReferralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/user")
public class ReferralController {

    private final ReferralService referralService;

    @Autowired
    public ReferralController(ReferralService referralService) {
        this.referralService = referralService;
    }

    @GetMapping("/referral/{id}")
    public List<Referral> getReferralForUser(@PathVariable Long id){
        return referralService.getAllReferralOfUser(id);
    }
}
