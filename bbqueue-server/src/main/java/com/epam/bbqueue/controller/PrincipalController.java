package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.repository.PrincipalRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class PrincipalController {
    private final PrincipalRepository principalRepository;

    public PrincipalController(PrincipalRepository principalRepository) {
        this.principalRepository = principalRepository;
    }

    @GetMapping("/principal/{principalId}")
    public ResponseEntity getPrincipalById(@PathVariable Long principalId) {
        Optional<Principal> optional = principalRepository.findById(principalId);
        if (optional.isPresent()) {
            return ResponseEntity.ok(optional.get());
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Not found!");
        }
    }

    @GetMapping("/principal-authenticated")
    public ResponseEntity getAuthenticatedPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Principal principal = (Principal) authentication.getPrincipal();
        return getPrincipalById(principal.getId());
    }
}
