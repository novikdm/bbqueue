package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.DoctorSpeciality;
import com.epam.bbqueue.service.interfaces.DoctorSpecialityService;
import com.epam.bbqueue.service.interfaces.HospitalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/speciality")
public class SearchSpecialityFormController {

    private final DoctorSpecialityService doctorSpecialityService;

    public SearchSpecialityFormController(DoctorSpecialityService doctorSpecialityService, HospitalService hospitalService) {
        this.doctorSpecialityService = doctorSpecialityService;
    }

    @GetMapping("/{specialityName}")
    public ResponseEntity getSpecialityByName(@PathVariable String specialityName) {
        DoctorSpeciality speciality = doctorSpecialityService.getDoctorSpecialityByName(specialityName);
        return ResponseEntity.ok(speciality);
    }
}