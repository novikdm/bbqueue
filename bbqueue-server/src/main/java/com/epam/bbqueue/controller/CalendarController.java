package com.epam.bbqueue.controller;

import com.epam.bbqueue.aop.annotation.Loggable;
import com.epam.bbqueue.exception.DoctorWorkDaysNotFoundException;
import com.epam.bbqueue.service.implementation.DoctorWorkDayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CalendarController {

  private final DoctorWorkDayServiceImpl doctorWorkDayService;

  @Autowired
  public CalendarController(DoctorWorkDayServiceImpl doctorWorkDayService) {
    this.doctorWorkDayService = doctorWorkDayService;
  }

  @Loggable
  @GetMapping("/calendar/{doctorId}")
  public List<Integer> getDoctorWorkDays(@PathVariable String doctorId) throws DoctorWorkDaysNotFoundException {
    long localDoctorId;
    try {
      localDoctorId = Long.parseLong(doctorId);
    } catch (NumberFormatException e) {
      throw new DoctorWorkDaysNotFoundException("Wrong doctor ID! ID must be an integer number!");
    }
    return doctorWorkDayService.getAllDoctorWorkDays(localDoctorId);
  }
}
