package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Region;
import com.epam.bbqueue.service.interfaces.RegionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/region")
public class SearchRegionFormController {

    private final RegionService regionService;

    public SearchRegionFormController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping("/{regionName}")
    public ResponseEntity getRegionByName(@PathVariable String regionName) {
        Region region = regionService.getRegionByName(regionName);
        return ResponseEntity.ok(region);
    }

    @GetMapping("/all")
    public ResponseEntity getAllRegionNames() {
        List<Region> regions = regionService.getAllRegions();
        List<String> regionNames = regions.stream().map(Region::getName).collect(Collectors.toList());
        return ResponseEntity.ok(regionNames);
    }


}
