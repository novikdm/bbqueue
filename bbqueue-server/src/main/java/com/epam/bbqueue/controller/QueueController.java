package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.entity.enums.Day;
import com.epam.bbqueue.service.implementation.QueueServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@SuppressWarnings("deprecation")
@RestController
@Slf4j
public class QueueController {

    private final QueueServiceImpl queueService;

    @Autowired
    public QueueController(QueueServiceImpl queueService) {
        this.queueService = queueService;
    }

    @GetMapping("/queue/free/{date}-{doctorId}")
    public List<Integer> getNumberOfFreeTimeSlots(@PathVariable String date,
                                                  @PathVariable long doctorId) throws ParseException {
        return queueService.getNumberOfFreeTimeSlots(date, doctorId);
    }

    @GetMapping("/queue/all/{date}-{doctorId}")
    public List<Integer> getListOfAllNumbersOfTimeSlot(@PathVariable String date,
                                                       @PathVariable long doctorId) throws ParseException {
        Date parsedDate = queueService.parseDate("yyyy-MM-dd", date);
        Day day = queueService.getDayByOrdinal(parsedDate.getDay());
        return queueService.getListOfAllNumbersOfTimeSlot(queueService.getDoctorWorkDayByDateAndId(day, doctorId));
    }

    @PostMapping("/queue")
    public TimeSlot postTimeSlot(@Valid @RequestBody TimeSlot timeSlot) {
        return queueService.addNewTimeSlot(timeSlot);
    }
}
