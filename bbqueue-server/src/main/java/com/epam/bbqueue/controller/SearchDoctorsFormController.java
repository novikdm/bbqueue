package com.epam.bbqueue.controller;

import com.epam.bbqueue.dto.DoctorSearchFormDto;
import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.service.interfaces.DoctorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/doctors")
public class SearchDoctorsFormController {

    private final DoctorService doctorService;

    public SearchDoctorsFormController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/in-hospital/{hospitalName}")
    public ResponseEntity getDoctorsByHospitalName(
            @PathVariable String hospitalName) {
        List<Doctor> doctors = doctorService.getDoctorsByHospitalName(hospitalName);
        List<DoctorSearchFormDto> doctorsDto = doctors.stream()
                .map(this::getDoctorDto).collect(Collectors.toList());
        return ResponseEntity.ok(doctorsDto);
    }

    @GetMapping("/with-speciality")
    public ResponseEntity getDoctorsBySpecialityAndHospital(
            @RequestParam String specialityName, Long hospitalId) {
        List<Doctor> doctors = doctorService
                .getDoctorsBySpecialityNameAndHospitalId(specialityName, hospitalId);
        List<DoctorSearchFormDto> doctorsDto = doctors.stream()
                .map(this::getDoctorDto).collect(Collectors.toList());
        return ResponseEntity.ok(doctorsDto);
    }

    @GetMapping("/with-speciality-and-name")
    public ResponseEntity getDoctorsBySpecialityAndHospitalAndDoctorName(
            @RequestParam Long specialityId, Long hospitalId, String lastName) {
        List<Doctor> doctors = doctorService.
                getDoctorsBySpecialityIdAndHospitalIdAndUserLastName(specialityId, hospitalId, lastName);
        List<DoctorSearchFormDto> doctorsDto = doctors.stream()
                .map(this::getDoctorDto).collect(Collectors.toList());
        return ResponseEntity.ok(doctorsDto);
    }

    @GetMapping("by-name")
    public ResponseEntity getDoctorsByHospitalAndDoctorName(@RequestParam Long hospitalId,
                                                            String lastName) {
        List<Doctor> doctors = doctorService
                .getDoctorsInHospitalByLastName(hospitalId, lastName);
        List<DoctorSearchFormDto> doctorsDto = doctors.stream()
                .map(this::getDoctorDto).collect(Collectors.toList());
        return ResponseEntity.ok(doctorsDto);
    }

    private DoctorSearchFormDto getDoctorDto(Doctor doctor) {
        return DoctorSearchFormDto
                .builder()
                .doctorId(doctor.getId())
                .cabinetNumber(doctor.getCabinetNumber())
                .firstName(doctor.getUser().getFirstName())
                .lastName(doctor.getUser().getLastName())
                .hospital(doctor.getHospital().getName())
                .speciality(doctor.getSpeciality().getName())
                .build();
    }

}
