package com.epam.bbqueue.aop.interfaces;

public interface Printable {
    void print(String message);
}
