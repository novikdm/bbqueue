package com.epam.bbqueue.aop.annotation;

import com.epam.bbqueue.entity.enums.LogLevels;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Loggable {
    LogLevels level() default LogLevels.INFO;
    String message() default "";
}