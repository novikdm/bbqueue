package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.DoctorSpeciality;

public interface DoctorSpecialityService {

    DoctorSpeciality getDoctorSpecialityByName(String specialityName);

}
