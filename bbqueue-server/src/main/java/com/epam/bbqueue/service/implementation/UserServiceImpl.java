package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.exception.InvalidUserDetailsException;
import com.epam.bbqueue.repository.UserRepository;
import com.epam.bbqueue.service.interfaces.PrincipalService;
import com.epam.bbqueue.service.interfaces.UserService;
import com.epam.bbqueue.service.validation.UserDetailsValidator;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserDetailsValidator userDetailsValidator;
    private final PrincipalService principalService;

    public UserServiceImpl(UserRepository userRepository, UserDetailsValidator userDetailsValidator,
                           PrincipalService principalService) {
        this.userRepository = userRepository;
        this.userDetailsValidator = userDetailsValidator;
        this.principalService = principalService;
    }

    @Override
    public void save(User user) {
        if (!userDetailsValidator.isNameValid(user.getFirstName())
                || !userDetailsValidator.isNameValid(user.getLastName())) {
            throw new InvalidUserDetailsException(String.format("User first or last name is invalid: [%s %s]",
                    user.getFirstName(), user.getLastName()));
        }
        principalService.save(user.getPrincipal());
        userRepository.save(user);
    }


}
