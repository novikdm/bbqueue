package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.User;

public interface UserService {
    void save(User user);
}
