package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.DoctorWorkDay;
import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.entity.enums.Day;
import com.epam.bbqueue.repository.DoctorWorkDayRepository;
import com.epam.bbqueue.repository.TimeSlotRepository;
import com.epam.bbqueue.service.interfaces.QueueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("deprecation")
@Service
@Slf4j
public class QueueServiceImpl implements QueueService {

    private final TimeSlotRepository timeSlotRepository;

    private final DoctorWorkDayRepository doctorWorkDayRepository;

    @Autowired
    public QueueServiceImpl(TimeSlotRepository timeSlotRepository, DoctorWorkDayRepository doctorWorkDayRepository) {
        this.timeSlotRepository = timeSlotRepository;
        this.doctorWorkDayRepository = doctorWorkDayRepository;
    }

    @Override
    public List<Integer> getNumberOfFreeTimeSlots(String date, Long doctorId) throws ParseException {
        Date parsedDate = parseDate("yyyy-MM-dd", date);
        Day day = getDayByOrdinal(parsedDate.getDay());
        DoctorWorkDay doctorWorkDay = getDoctorWorkDayByDateAndId(day, doctorId);
        List<Integer> listOfNumberTimeSlots = getListOfAllNumbersOfTimeSlot(doctorWorkDay);
        timeSlotRepository.findAllByVisitDateAndService_Doctor_Id(parsedDate, doctorId)
                .forEach(workDay -> listOfNumberTimeSlots.removeAll(
                        IntStream.rangeClosed(workDay.getTimeSlotNumber(),
                                workDay.getTimeSlotNumber() + workDay.getService().getTimeSlotAmount() - 1)
                                .boxed()
                                .collect(Collectors.toSet())));
        return listOfNumberTimeSlots;
    }

    public Day getDayByOrdinal(int ordinal) {
        return Day.getDayByOrdinal(ordinal)
                .orElseThrow(() -> new NoSuchElementException(ordinal + " doesn't exist"));
    }

    public DoctorWorkDay getDoctorWorkDayByDateAndId(Day day, long id) {
        return doctorWorkDayRepository.findByDayAndDoctor_Id(day, id)
                .orElseThrow(() -> new NoSuchElementException("There is no such a doctor works in " + day));
    }

    public Date parseDate(String pattern, String date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.parse(date);
    }

    @Override
    public List<Integer> getListOfAllNumbersOfTimeSlot(DoctorWorkDay doctorWorkDay) {
        int numberOfTimeSlots = (int) (TimeUnit.MINUTES.convert(
                doctorWorkDay.getWorkEndTime().getTime() -
                        doctorWorkDay.getWorkStartTime().getTime(), TimeUnit.MILLISECONDS) / 15);
        int numberOfLunchTimeSlots = (int) (TimeUnit.MINUTES.convert(
                doctorWorkDay.getLunchEndTime().getTime() -
                        doctorWorkDay.getLunchStartTime().getTime(), TimeUnit.MILLISECONDS) / 15);
        int lunchStartPositionNumber = (int) (TimeUnit.MINUTES.convert(
                doctorWorkDay.getLunchStartTime().getTime() -
                        doctorWorkDay.getWorkStartTime().getTime(), TimeUnit.MILLISECONDS) / 15);
        List<Integer> listOfNumberTimeSlots = IntStream
                .rangeClosed(1, numberOfTimeSlots)
                .boxed()
                .collect(Collectors.toList());
        listOfNumberTimeSlots.removeAll(IntStream
                .rangeClosed(lunchStartPositionNumber + 1, numberOfLunchTimeSlots + lunchStartPositionNumber)
                .boxed()
                .collect(Collectors.toSet()));
        return listOfNumberTimeSlots;
    }

    @Override
    @Transactional
    public TimeSlot addNewTimeSlot(TimeSlot timeSlot) {
        List<TimeSlot> listOfNumberTimeSlots = timeSlotRepository.findAllByVisitDateAndService_Doctor_Id(
                timeSlot.getVisitDate(), timeSlot.getService().getDoctor().getId());
        List<Integer> necessaryTimeSlotsNumberForBooking = IntStream
                .range(timeSlot.getTimeSlotNumber(),
                        timeSlot.getTimeSlotNumber() + timeSlot.getService().getTimeSlotAmount())
                .boxed()
                .collect(Collectors.toList());
        long isAlreadyBooked = listOfNumberTimeSlots
                .stream()
                .filter(ts -> necessaryTimeSlotsNumberForBooking.contains(ts.getTimeSlotNumber()))
                .count();
        if (isAlreadyBooked != 0) {
            throw new RuntimeException();
        }
        return timeSlotRepository.saveAndFlush(timeSlot);
    }
}
