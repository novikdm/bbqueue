package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Referral;
import com.epam.bbqueue.repository.ReferralRepository;
import com.epam.bbqueue.service.interfaces.ReferralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReferralServiceImpl implements ReferralService {

    private final ReferralRepository referralRepository;

    @Autowired
    public ReferralServiceImpl(ReferralRepository referralRepository) {
        this.referralRepository = referralRepository;
    }

    @Override
    public List<Referral> getAllReferralOfUser(Long id) {
        return referralRepository.findAllByCustomer_Id(id);
    }
}
