package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.Referral;

import java.util.List;

public interface ReferralService {

    List<Referral> getAllReferralOfUser(Long id);

}
