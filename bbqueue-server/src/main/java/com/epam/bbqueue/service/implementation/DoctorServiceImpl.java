package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.exception.DoctorNotFoundException;
import com.epam.bbqueue.repository.DoctorRepository;
import com.epam.bbqueue.service.interfaces.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorServiceImpl(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    public List<Doctor> getAllDoctors() {
        return doctorRepository.findAll();
    }

    @Override
    public List<Doctor> getDoctorsByHospitalName(String hospitalName) {
        List<Doctor> doctors = doctorRepository.findAllByHospital_Name(hospitalName);
        if (doctors.isEmpty()) {
            throw new DoctorNotFoundException(String.format
                    ("In the hospital [%s] not found doctors!", hospitalName));
        }
        return doctors;
    }

    @Override
    public List<Doctor> getDoctorsInHospitalByLastName(Long hospitalId, String lastName) {
        List<Doctor> doctors = doctorRepository
                .findAllByHospital_IdAndUser_LastName(hospitalId, lastName);
        if (doctors.isEmpty()) {
            throw new DoctorNotFoundException(String.format
                    ("In the hospital with [%s] not found doctors with name [%s]!", hospitalId));
        }
        return doctors;
    }

    @Override
    public List<Doctor> getDoctorsBySpecialityNameAndHospitalId(String specialityName, Long hospitalId) {
        List<Doctor> doctors = doctorRepository
                .findAllBySpeciality_NameAndHospital_Id(specialityName, hospitalId);
        if (doctors.isEmpty()) {
            throw new DoctorNotFoundException(String.format
                    ("Not found doctors with speciality name [%s] and hospital id [%s]!",
                            specialityName, hospitalId));
        }
        return doctors;
    }

    @Override
    public List<Doctor> getDoctorsBySpecialityIdAndHospitalIdAndUserLastName(Long specialityId,
                                                                             Long hospitalId,
                                                                             String lastName) {
        List<Doctor> doctors = doctorRepository
                .findAllBySpeciality_IdAndHospital_IdAndUser_LastName(specialityId, hospitalId, lastName);
        if (doctors.isEmpty()) {
            throw new DoctorNotFoundException(String.format
                    ("Not found doctors with speciality id [%s] and hospital id [%s] and name [%s]!",
                            specialityId, hospitalId, lastName));
        }
        return doctors;
    }

}

