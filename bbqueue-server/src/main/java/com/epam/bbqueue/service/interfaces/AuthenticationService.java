package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.dto.JwtResponse;
import com.epam.bbqueue.exception.*;

public interface AuthenticationService {
    JwtResponse attemptLogin(String username, String password);
    void verifyEmail(String email, String code);
    void sendEmailVerificationCode(String email);
    void changePassword(String email, String password, String code);
}
