package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.DoctorSpeciality;
import com.epam.bbqueue.exception.DoctorSpecialityNotFoundException;
import com.epam.bbqueue.repository.DoctorSpecialityRepository;
import com.epam.bbqueue.service.interfaces.DoctorSpecialityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DoctorSpecialityServiceImpl implements DoctorSpecialityService {

    private final DoctorSpecialityRepository doctorSpecialityRepository;

    @Autowired
    public DoctorSpecialityServiceImpl(
            DoctorSpecialityRepository doctorSpecialityRepository) {
        this.doctorSpecialityRepository = doctorSpecialityRepository;
    }

    @Override
    public DoctorSpeciality getDoctorSpecialityByName(String specialityName) {
        return doctorSpecialityRepository.findByNameContaining(specialityName)
                .orElseThrow(() -> new DoctorSpecialityNotFoundException(String.format
                        ("Not found the speciality with name [%s]", specialityName)));
    }
}
