package com.epam.bbqueue.service.validation;

import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UserDetailsValidator {
    private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}";
    private static final String CYRILLIC_NAME_PATTERN = "^[а-яєіїА-ЯЄІЇ]+$";
    private static final String LATIN_NAME_PATTERN = "^[a-zA-Z]+$";
    private static final Pattern EMAIL_PATTERN =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public boolean isNameValid(String name) {
        return ((name.matches(CYRILLIC_NAME_PATTERN) || (name.matches(LATIN_NAME_PATTERN)))
                && (name.length() < 40));
    }

    public boolean isPasswordValid(String password) {
        return password.matches(PASSWORD_PATTERN);
    }

    public boolean isEmailValid(String email) {
        return EMAIL_PATTERN.matcher(email).find();
    }
}
