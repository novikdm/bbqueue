package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.DoctorWorkDay;
import com.epam.bbqueue.entity.TimeSlot;

import java.text.ParseException;
import java.util.List;

public interface QueueService {
    List<Integer> getNumberOfFreeTimeSlots(String date, Long doctorId) throws ParseException;

    TimeSlot addNewTimeSlot(TimeSlot timeSlot);

    List<Integer> getListOfAllNumbersOfTimeSlot(DoctorWorkDay doctorWorkDay);
}
