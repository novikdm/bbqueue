package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.repository.PrincipalRepository;
import com.epam.bbqueue.service.interfaces.UserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final PrincipalRepository principalRepository;

    public UserDetailsServiceImpl(PrincipalRepository principalRepository) {
        this.principalRepository = principalRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) {
        return principalRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " was not found"));
    }
}
