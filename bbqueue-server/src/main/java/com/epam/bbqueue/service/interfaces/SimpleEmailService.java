package com.epam.bbqueue.service.interfaces;

public interface SimpleEmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
