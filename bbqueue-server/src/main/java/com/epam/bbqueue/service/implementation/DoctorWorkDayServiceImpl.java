package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.exception.DoctorWorkDaysNotFoundException;
import com.epam.bbqueue.repository.DoctorWorkDayRepository;
import com.epam.bbqueue.service.interfaces.DoctorWorkDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DoctorWorkDayServiceImpl implements DoctorWorkDayService {

  private final DoctorWorkDayRepository doctorWorkDayRepository;

  @Autowired
  public DoctorWorkDayServiceImpl(DoctorWorkDayRepository doctorWorkDayRepository) {
    this.doctorWorkDayRepository = doctorWorkDayRepository;
  }

  @Override
  public List<Integer> getAllDoctorWorkDays(long doctorID) {
    List<Integer> doctorWorkDays = doctorWorkDayRepository.findAllByDoctorId(doctorID).stream()
            .map(doctorWorkDay -> doctorWorkDay.getDay().ordinal() + 1).sorted()
            .collect(Collectors.toList());
    if(doctorWorkDays.isEmpty()) {
      throw new DoctorWorkDaysNotFoundException("Work days doesn't exist for this doctor(doctorID = " + doctorID + ")!");
    } else return doctorWorkDays;
  }
}
