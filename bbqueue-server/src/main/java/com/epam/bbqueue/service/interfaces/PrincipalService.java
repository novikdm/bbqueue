package com.epam.bbqueue.service.interfaces;

import com.epam.bbqueue.entity.Principal;

public interface PrincipalService {
    void save(Principal principal);

    void setNewPassword(String email, String newPassword);
}
