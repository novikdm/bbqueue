package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.Role;
import com.epam.bbqueue.entity.enums.RoleName;
import com.epam.bbqueue.exception.InvalidPrincipalException;
import com.epam.bbqueue.exception.OccupiedEmailException;
import com.epam.bbqueue.exception.UserNotFoundException;
import com.epam.bbqueue.repository.PrincipalRepository;
import com.epam.bbqueue.repository.RoleRepository;
import com.epam.bbqueue.service.interfaces.PrincipalService;
import com.epam.bbqueue.service.validation.UserDetailsValidator;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;

@Service
public class PrincipalServiceImpl implements PrincipalService {
    private final RoleRepository roleRepository;
    private final UserDetailsValidator userDetailsValidator;
    private final PrincipalRepository principalRepository;
    private final PasswordEncoder passwordEncoder;

    public PrincipalServiceImpl(PrincipalRepository principalRepository, PasswordEncoder passwordEncoder,
                                UserDetailsValidator userDetailsValidator, RoleRepository roleRepository) {
        this.principalRepository = principalRepository;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsValidator = userDetailsValidator;
        this.roleRepository = roleRepository;
    }

    @Override
    public void save(Principal principal) {
        if (!userDetailsValidator.isEmailValid(principal.getEmail())
                || !userDetailsValidator.isPasswordValid(principal.getPassword())) {
            throw new InvalidPrincipalException(String.format("Invalid email or password: [%s]",
                    principal.toString()));
        } else if (principalRepository.existsByEmail(principal.getEmail())) {
            throw new OccupiedEmailException(String.format("Such email is already occupied: [%s]",
                    principal.getEmail()));
        }
        encodePassword(principal);
        updatePrincipalDetails(principal);
        principalRepository.save(principal);
    }

    private void updatePrincipalDetails(Principal principal) {
        if (principal.getRoles() == null) {
            principal.setRoles(new HashSet<>());
        }
        if (principal.getRoles().isEmpty()
                || principal.getRoles().stream().anyMatch(role -> role.getName().equals(RoleName.USER))) {
            Optional<Role> optional = roleRepository.findByName(RoleName.USER);
            Role role;
            if (optional.isPresent()) {
                role = optional.get();
            } else {
                role = new Role();
                role.setName(RoleName.USER);
                roleRepository.save(role);
            }
            principal.getRoles().add(role);
        }
        principal.setActivated(false);
    }

    private void encodePassword(Principal principal) {
        principal.setPassword(passwordEncoder.encode(principal.getPassword()));
    }

    @Override
    public void setNewPassword(String email, String newPassword) {
        Principal principal = principalRepository.findByEmail(email)
                .orElseThrow(() ->
                new UserNotFoundException(String.format("Not found principal for email [%s]", email)));
        if (!userDetailsValidator.isPasswordValid(newPassword)) {
            throw new InvalidPrincipalException(String.format("Invalid password: [%s]",
                    newPassword));
        }
        principal.setPassword(newPassword);
        encodePassword(principal);
        principalRepository.save(principal);
    }
}
