package com.epam.bbqueue.service.interfaces;

import java.util.List;

public interface DoctorWorkDayService {

  List<Integer> getAllDoctorWorkDays(long doctorID);

}
