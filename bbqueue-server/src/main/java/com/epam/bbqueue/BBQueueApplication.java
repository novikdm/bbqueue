package com.epam.bbqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BBQueueApplication {
    public static void main(String[] args) {
        SpringApplication.run(BBQueueApplication.class, args);

    }
}