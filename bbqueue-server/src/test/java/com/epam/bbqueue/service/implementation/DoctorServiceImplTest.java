package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.repository.DoctorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DoctorServiceImpl.class})
public class DoctorServiceImplTest {

    @MockBean
    private DoctorRepository doctorRepository;

    @Autowired
    private DoctorServiceImpl doctorServiceImpl;

    private List<Doctor> actualResult = new ArrayList<>();
    private List<Doctor> expectedResult = new ArrayList<>();

    @Before
    public void initially() {
        Doctor doc = new Doctor();
        doc.setId(1L);
        expectedResult = Arrays.asList(doc);

        when(doctorRepository.findAll()).thenReturn(expectedResult);

        when(doctorRepository.findAllByHospital_Name("HospitalName"))
                .thenReturn(expectedResult);
    }

    @Test
    public void testGetAllDoctors() {

        actualResult = doctorServiceImpl.getAllDoctors();

        assertEquals(1, actualResult.size());
        assertThat(actualResult).isEqualTo(expectedResult);

    }

    @Test
    public void testGetAllDoctorsByHospitalName() {

        actualResult = doctorServiceImpl.getDoctorsByHospitalName("HospitalName");

        assertEquals(1, actualResult.size());
        assertThat(actualResult).isEqualTo(expectedResult);
    }

}