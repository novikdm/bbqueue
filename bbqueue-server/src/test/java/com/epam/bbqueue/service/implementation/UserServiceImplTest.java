package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.repository.PrincipalRepository;
import com.epam.bbqueue.repository.UserRepository;
import com.epam.bbqueue.service.interfaces.PrincipalService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {
    @Autowired
    private UserServiceImpl userService;
    @MockBean
    private PrincipalService mockPrincipalService;
    @MockBean
    private PrincipalRepository mockPrincipalRepository;
    @MockBean
    private UserRepository mockUserRepository;

    @Test
    public void save() {
        String firstName = "Mykhailo";
        String lastName = "Jirnii";
        String email = "example@email.com";
        String password = "123456Ss";
        User testUser = getTestUser(firstName, lastName, email, password);
        long expected = 1L;
        mockServices(testUser, expected);
        userService.save(testUser);
        assertThat(testUser.getId()).isEqualTo(expected);
    }

    private void mockServices(User testUser, long returningValue) {
        when(mockPrincipalRepository.existsByEmail(testUser.getPrincipal().getEmail()))
                .thenReturn(false);
        doAnswer((invocationOnMock) -> null)
                .when(mockPrincipalService).save(testUser.getPrincipal());
        doAnswer((invocationOnMock) -> {
            User user = invocationOnMock.getArgument(0);
            user.setId(returningValue);
            return user;
        }).when(mockUserRepository).save(testUser);
        when(mockUserRepository.save(testUser))
                .thenReturn(testUser);
    }

    private User getTestUser(String firstName, String lastName, String email, String password) {
        return User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .principal(Principal.builder().email(email).password(password).build())
                .build();
    }
}