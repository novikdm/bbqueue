package com.epam.bbqueue.service;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.entity.Referral;
import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.repository.ReferralRepository;
import com.epam.bbqueue.service.implementation.ReferralServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ReferralServiceImpl.class})
@Slf4j
public class ReferralServiceTest {

    @Autowired
    private ReferralServiceImpl referralService;

    @MockBean
    private ReferralRepository referralRepository;

    private Referral createReferral(){
        Referral referral = new Referral();

        Doctor doctorFrom = new Doctor();
        doctorFrom.setId(5L);
        Doctor doctorTo = new Doctor();
        doctorTo.setId(6L);
        User user = new User();
        user.setId(1L);

        referral.setId(1L);
        referral.setDescription("test referral");
        referral.setDoctorFrom(doctorFrom);
        referral.setDoctorTo(doctorTo);
        referral.setCustomer(user);

        return referral;
    }

    @Test
    public void test(){

    }
}
