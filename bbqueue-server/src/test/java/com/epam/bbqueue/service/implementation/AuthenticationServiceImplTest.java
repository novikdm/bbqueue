package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.config.security.jwt.JwtProvider;
import com.epam.bbqueue.dto.JwtResponse;
import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.VerificationToken;
import com.epam.bbqueue.exception.UserNotFoundException;
import com.epam.bbqueue.repository.PrincipalRepository;
import com.epam.bbqueue.repository.VerificationTokenRepository;
import com.epam.bbqueue.service.interfaces.PrincipalService;
import com.epam.bbqueue.service.interfaces.SimpleEmailService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class AuthenticationServiceImplTest {
    @Autowired
    private AuthenticationServiceImpl authenticationService;

    @MockBean
    private AuthenticationManager mockAuthenticationManager;
    @MockBean
    private JwtProvider jwtProvider;
    @MockBean
    private PrincipalRepository mockPrincipalRepository;
    @MockBean
    private PrincipalService mockPrincipalService;
    @MockBean
    private VerificationTokenRepository mockVerificationTokenRepository;
    @MockBean
    private SimpleEmailService simpleEmailService;

    @Test
    public void attemptLogin() {
        String email = "some@email.com";
        JwtResponse expectedResponse = new JwtResponse("thisisaccesstokenfortestuser");
        Principal principal = getTestPrincipal(email, true);
        String password = principal.getPassword();
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(principal, null);
        when(mockPrincipalRepository.existsByEmail(email))
                .thenReturn(true);
        when(mockAuthenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password)
        )).thenReturn(authenticationToken);
        when(jwtProvider.generateJwtToken(authenticationToken))
                .thenReturn(expectedResponse.getAccessToken());

        JwtResponse actualResponse = authenticationService.attemptLogin(email, password);
        assertThat(actualResponse).isEqualTo(expectedResponse);
    }

    @Test
    public void verifyEmail() {
        String email = "example@email.com";
        Principal principal = getTestPrincipal(email, false);
        String token = principal.getVerificationToken().getToken();
        when(mockPrincipalRepository.findByEmail(email))
                .thenReturn(Optional.of(principal));
        long expected = 1L;
        doAnswer(invocationOnMock -> {
            VerificationToken verificationToken = invocationOnMock.getArgument(0);
            verificationToken.setId(expected);
            return verificationToken;
        }).when(mockVerificationTokenRepository).save(principal.getVerificationToken());
        authenticationService.verifyEmail(email, token);
        assertThat(principal.getVerificationToken().getId()).isEqualTo(expected);
    }

    @Test
    public void sendEmailVerificationCode() throws UserNotFoundException {
        String email = "example@email.com";
        doAnswer(invocationOnMock -> null)
                .when(simpleEmailService).sendSimpleMessage(eq(email), anyString(), anyString());
        Principal principal = Principal.builder()
                .email(email)
                .build();
        when(mockPrincipalRepository.findByEmail(email))
                .thenReturn(Optional.of(principal));
        doAnswer(invocationOnMock -> {
            VerificationToken argument = invocationOnMock.getArgument(0);
            Principal actual = argument.getPrincipal();
            assertThat(actual).isEqualTo(principal);
            return argument;
        }).when(mockVerificationTokenRepository).save(any());
        authenticationService.sendEmailVerificationCode(email);
    }

    private Principal getTestPrincipal(String email, boolean isActivated) {
        String token = UUID.randomUUID().toString();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -8);
        Date issuedDateTime = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date expiredDateTime = calendar.getTime();
        return Principal.builder()
                .email(email)
                .password("123456Ss")
                .isActivated(isActivated)
                .verificationToken(VerificationToken.builder()
                        .issuedDateTime(issuedDateTime)
                        .expiredDateTime(expiredDateTime)
                        .token(token)
                        .build())
                .build();
    }

    @Test
    public void changePassword() {
        String email = "example@email.com";
        String password = "123456Ss";
        String code = UUID.randomUUID().toString();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -8);
        Date issuedDateTime = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date expiredDateTime = calendar.getTime();

        Principal principal = Principal.builder()
                .isActivated(true)
                .id(1L)
                .email(email)
                .password("oldPassword")
                .verificationToken(VerificationToken.builder()
                        .id(1L)
                        .isConfirmed(false)
                        .token(code)
                        .issuedDateTime(issuedDateTime)
                        .expiredDateTime(expiredDateTime)
                        .build())
                .build();
        when(mockPrincipalRepository.findByEmail(email))
                .thenReturn(Optional.of(principal));
        doAnswer(invocationOnMock -> null)
                .when(mockPrincipalService).setNewPassword(email, password);
        doAnswer(invocationOnMock -> {
            VerificationToken argument = invocationOnMock.getArgument(0);
            assertThat(argument.isConfirmed()).isTrue();
            return argument;
        }).when(mockVerificationTokenRepository).save(any());
        authenticationService.changePassword(email, password, code);
    }
}