package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Region;
import com.epam.bbqueue.repository.RegionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {RegionServiceImpl.class})
public class RegionServiceImplTest {

    @MockBean
    private RegionRepository regionRepository;

    @Autowired
    private RegionServiceImpl regionService;

    private Region actualResult = new Region();
    private List<Region> expectedList = new ArrayList<>();
    private Region expectedResult = new Region();

    @Before
    public void initially() {
        expectedResult.setName("Region");
        expectedList = Arrays.asList(expectedResult);

        when(regionRepository.findAll()).thenReturn(expectedList);

        when(regionRepository.findByNameContaining(expectedResult.getName()))
                .thenReturn(Optional.of(expectedResult));

    }


    @Test
    public void getRegionByName() {

        actualResult = regionService.getRegionByName("Region");

        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void getAllRegions() {

        List<Region> actualList = regionService.getAllRegions();

        assertEquals(1, actualList.size());
        assertThat(actualList).isEqualTo(expectedList);
    }
}