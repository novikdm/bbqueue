package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.DoctorSpeciality;
import com.epam.bbqueue.repository.DoctorSpecialityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DoctorSpecialityServiceImpl.class})
public class DoctorSpecialityServiceImplTest {

    @MockBean
    private DoctorSpecialityRepository specialityRepository;

    @Autowired
    private DoctorSpecialityServiceImpl specialityService;

    private DoctorSpeciality actualResult = new DoctorSpeciality();
    private List<DoctorSpeciality> expectedList = new ArrayList<>();
    private DoctorSpeciality expectedResult = new DoctorSpeciality();

    @Before
    public void initially() {
        expectedResult.setName("Speciality");
        expectedList = Arrays.asList(expectedResult);

        when(specialityRepository.findByNameContaining(expectedResult.getName()))
                .thenReturn(Optional.of(expectedResult));

    }


    @Test
    public void getDoctorSpecialityByName() {

        actualResult = specialityService.getDoctorSpecialityByName("Speciality");

        assertEquals(expectedResult, actualResult);

    }
}