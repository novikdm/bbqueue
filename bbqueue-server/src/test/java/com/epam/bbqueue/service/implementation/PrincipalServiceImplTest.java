package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.Role;
import com.epam.bbqueue.entity.enums.RoleName;
import com.epam.bbqueue.repository.PrincipalRepository;
import com.epam.bbqueue.repository.RoleRepository;
import com.epam.bbqueue.service.interfaces.SimpleEmailService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PrincipalServiceImplTest {
    @Autowired
    private PrincipalServiceImpl principalService;
    @MockBean
    private SimpleEmailService simpleEmailService;
    @MockBean
    private PrincipalRepository mockPrincipalRepository;
    @MockBean
    private RoleRepository roleRepository;

    @Test
    public void save() {
        String email = "example@email.com";
        String password = "123456Ss";
        doAnswer(invocationOnMock -> null)
                .when(simpleEmailService).sendSimpleMessage(eq(email), anyString(), anyString());
        Principal principal = Principal.builder()
                .email(email)
                .password(password)
                .build();
        Role role = new Role();
        role.setId(1L);
        role.setName(RoleName.USER);
        long expected = 1L;
        doAnswer(invocationOnMock -> {
            Principal argument = invocationOnMock.getArgument(0);
            argument.setId(expected);
            return argument;
        }).when(mockPrincipalRepository).save(principal);
        when(roleRepository.findByName(RoleName.USER))
                .thenReturn(Optional.of(role));
        when(mockPrincipalRepository.existsByEmail(email))
                .thenReturn(false);
        principalService.save(principal);
        assertThat(principal.getId()).isEqualTo(expected);
    }

    @Test
    public void setNewPassword() {
        String email = "example@email.com";
        String password = "123456Ss";
        String oldPassword = "oldpassword";
        Principal principal = Principal.builder()
                .email(email)
                .password(oldPassword)
                .build();
        when(mockPrincipalRepository.findByEmail(email))
                .thenReturn(Optional.of(principal));
        doAnswer(invocationOnMock -> {
            Principal argument = invocationOnMock.getArgument(0);
            log.info(principal.toString());
            assertThat(argument.getPassword())
                    .isNotEqualTo(oldPassword);
            return argument;
        }).when(mockPrincipalRepository).save(any());
        principalService.setNewPassword(email,password);
    }
}