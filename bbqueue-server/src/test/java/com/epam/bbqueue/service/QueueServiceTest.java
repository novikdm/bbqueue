package com.epam.bbqueue.service;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.entity.DoctorService;
import com.epam.bbqueue.entity.DoctorWorkDay;
import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.entity.enums.Day;
import com.epam.bbqueue.repository.DoctorWorkDayRepository;
import com.epam.bbqueue.repository.TimeSlotRepository;
import com.epam.bbqueue.service.implementation.QueueServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@SuppressWarnings("ALL")
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {QueueServiceImpl.class})
@Slf4j
public class QueueServiceTest {

    @Autowired
    private QueueServiceImpl queueService;

    @MockBean
    private TimeSlotRepository timeSlotRepository;

    @MockBean
    private DoctorWorkDayRepository doctorWorkDayRepository;

    @Before
    public void initTimeSlotRepository() throws ParseException {
        Date parsedDate = queueService.parseDate("yyyy-MM-dd", "2019-09-05");

        when(timeSlotRepository.findAllByVisitDateAndService_Doctor_Id(parsedDate, 1L))
                .thenReturn(Arrays.asList(
                        createTimeSlot(1, 3),
                        createTimeSlot(4, 2),
                        createTimeSlot(7, 1),
                        createTimeSlot(19, 2)));

        when(timeSlotRepository.saveAndFlush(createTimeSlot(6, 1)))
                .thenReturn(createTimeSlot(6, 1));
    }

    private TimeSlot createTimeSlot(Integer timeSlotNumber, Integer timeSlotLength) throws ParseException {
        TimeSlot timeSlot = new TimeSlot();

        Doctor doctor = new Doctor();
        doctor.setId(1L);

        DoctorService doctorService = new DoctorService();
        doctorService.setTimeSlotAmount(timeSlotLength);
        doctorService.setDoctor(doctor);

        Date date = queueService.parseDate("yyyy-MM-dd", "2019-09-05");

        timeSlot.setVisitDate(date);
        timeSlot.setService(doctorService);
        timeSlot.setTimeSlotNumber(timeSlotNumber);

        return timeSlot;
    }

    @Before
    public void initDoctorWorkDayRepository() {
        DoctorWorkDay doctorWorkDay = new DoctorWorkDay();
        doctorWorkDay.setDay(Day.THURSDAY);

        Date dateStartTimeHour = new Date();
        dateStartTimeHour.setHours(10);
        dateStartTimeHour.setMinutes(0);

        Date dateStartTimeLunch = new Date();
        dateStartTimeLunch.setHours(13);
        dateStartTimeLunch.setMinutes(0);

        Date dateEndTimeLunch = new Date();
        dateEndTimeLunch.setHours(14);
        dateEndTimeLunch.setMinutes(30);

        Date dateEndTimeHour = new Date();
        dateEndTimeHour.setHours(17);
        dateEndTimeHour.setMinutes(30);

        doctorWorkDay.setWorkStartTime(dateStartTimeHour);
        doctorWorkDay.setWorkEndTime(dateEndTimeHour);
        doctorWorkDay.setLunchStartTime(dateStartTimeLunch);
        doctorWorkDay.setLunchEndTime(dateEndTimeLunch);
        when(doctorWorkDayRepository.findByDayAndDoctor_Id(Day.THURSDAY, 1L))
                .thenReturn(Optional.of(doctorWorkDay));
    }

    @Test
    public void testGetNumberOfFreeTimeslots() throws ParseException {
        List<Integer> freeTimeSlots = queueService.getNumberOfFreeTimeSlots("2019-09-05", 1L);
        Assert.assertEquals(16, freeTimeSlots.size());
        Assert.assertTrue(!freeTimeSlots.contains(1));  // 10:00 - 10:45
        Assert.assertTrue(!freeTimeSlots.contains(2));
        Assert.assertTrue(!freeTimeSlots.contains(3));
        Assert.assertTrue(!freeTimeSlots.contains(4));  // 10:45 - 11:15
        Assert.assertTrue(!freeTimeSlots.contains(5));
        Assert.assertTrue(!freeTimeSlots.contains(7));  // 11:30 - 11:45
        Assert.assertTrue(!freeTimeSlots.contains(13)); // 13:00 - 14:30
        Assert.assertTrue(!freeTimeSlots.contains(14));
        Assert.assertTrue(!freeTimeSlots.contains(15));
        Assert.assertTrue(!freeTimeSlots.contains(16));
        Assert.assertTrue(!freeTimeSlots.contains(17));
        Assert.assertTrue(!freeTimeSlots.contains(18));
        Assert.assertTrue(!freeTimeSlots.contains(19)); // 14:30 - 15:00
        Assert.assertTrue(!freeTimeSlots.contains(20));
    }

    @Test
    public void testGetListOfAllNumbersOfTimeSlot() {
        Assert.assertEquals(24, queueService.getListOfAllNumbersOfTimeSlot(
                doctorWorkDayRepository.findByDayAndDoctor_Id(Day.THURSDAY, 1L).get()).size());
    }

    @Test
    public void testParseDate() throws ParseException {
        Date date = queueService.parseDate("yyyy-MM-dd", "2019-09-05");
        Assert.assertEquals(4, date.getDay());
        Assert.assertEquals(5, date.getDate());
        Assert.assertEquals(9, date.getMonth() + 1);
        Assert.assertEquals(2019, date.getYear() + 1900);
    }

    @Test
    public void testAddNewTimeSlot() throws ParseException {
        TimeSlot requestedTimeSlot = createTimeSlot(6, 1);
        Assert.assertEquals(6, queueService.addNewTimeSlot(requestedTimeSlot).getTimeSlotNumber().intValue());
    }

    @Test(expected = RuntimeException.class)
    public void testAddNewTimeSlot_Should_Fail_No_Place() throws ParseException {
        queueService.addNewTimeSlot(createTimeSlot(6, 2));
    }
}
