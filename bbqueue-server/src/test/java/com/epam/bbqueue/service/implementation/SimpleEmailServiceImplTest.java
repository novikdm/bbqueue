package com.epam.bbqueue.service.implementation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doAnswer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleEmailServiceImplTest {
    @Autowired
    private SimpleEmailServiceImpl simpleEmailService;
    @MockBean
    private JavaMailSender javaMailSender;

    @Test
    public void sendSimpleMessage() {
        String to = "example@test-email.com";
        String subject = "Test subject";
        String text = "Test text";
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(to);
        mailMessage.setSubject(subject);
        mailMessage.setText(text);
        AtomicBoolean success = new AtomicBoolean(false);
        doAnswer((Answer<Void>) invocationOnMock -> {
            success.set(true);
            return null;
        }).when(javaMailSender).send(mailMessage);
        simpleEmailService.sendSimpleMessage(to, subject, text);
        assertThat(success).isTrue();
    }
}