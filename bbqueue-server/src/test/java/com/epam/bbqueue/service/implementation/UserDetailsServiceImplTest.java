package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.repository.PrincipalRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDetailsServiceImplTest {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @MockBean
    private PrincipalRepository mockPrincipalRepository;

    @Test
    public void loadUserByUsername() {
        String email = "example@mail.com";
        Principal expected = Principal.builder()
                .password("123456Ss")
                .email(email)
                .build();
        when(mockPrincipalRepository.findByEmail(email))
                .thenReturn(Optional.of(expected));
        UserDetails actual = userDetailsService.loadUserByUsername(email);
        assertThat(actual).isEqualTo(expected);
    }
}