package com.epam.bbqueue.service.implementation;

import com.epam.bbqueue.entity.Doctor;
import com.epam.bbqueue.entity.DoctorWorkDay;
import com.epam.bbqueue.entity.enums.Day;
import com.epam.bbqueue.exception.DoctorWorkDaysNotFoundException;
import com.epam.bbqueue.repository.DoctorWorkDayRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DoctorWorkDayServiceImpl.class})
public class DoctorWorkDayServiceImplTest {

  private DoctorWorkDayServiceImpl doctorWorkDayService;

  @MockBean
  private DoctorWorkDayRepository doctorWorkDayRepository;

  @Before
  public void initDoctorWorkDayServiceImpl() {
    doctorWorkDayService = new DoctorWorkDayServiceImpl(doctorWorkDayRepository);
    when(doctorWorkDayRepository.findAllByDoctorId(1L)).thenReturn(getMockDoctorWorkDayList());
    when(doctorWorkDayRepository.findAllByDoctorId(-1L)).thenReturn(new ArrayList<>());
  }

  @Test
  public void getAllDoctorWorkDaysTestWithLegalId() {
    assertEquals(Arrays.asList(2, 5), doctorWorkDayService.getAllDoctorWorkDays(1L));
  }

  @Test(expected = DoctorWorkDaysNotFoundException.class)
  public void getAllDoctorWorkDaysTestWithIllegalId() {
    doctorWorkDayService.getAllDoctorWorkDays(-1L);
  }

  private List<DoctorWorkDay> getMockDoctorWorkDayList() {
    Doctor doctor = new Doctor();
    DoctorWorkDay workDayFriday = new DoctorWorkDay();
    DoctorWorkDay workDayTuesday = new DoctorWorkDay();
    DoctorWorkDay[] doctorWorkDayArray = new DoctorWorkDay[2];
    doctor.setId(1L);
    workDayFriday.setDoctor(doctor);
    workDayFriday.setDay(Day.FRIDAY);
    doctorWorkDayArray[0] = workDayFriday;
    workDayTuesday.setDoctor(doctor);
    workDayTuesday.setDay(Day.TUESDAY);
    doctorWorkDayArray[1] = workDayTuesday;
    return Arrays.asList(doctorWorkDayArray);
  }




}
