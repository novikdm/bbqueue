package com.epam.bbqueue.controller;

import com.epam.bbqueue.dto.*;
import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.User;
import com.epam.bbqueue.service.interfaces.AuthenticationService;
import com.epam.bbqueue.service.interfaces.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthenticationControllerTest {
    @Autowired
    private AuthenticationController authenticationController;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @MockBean
    private AuthenticationService mockAuthenticationService;
    @MockBean
    private UserService mockUserService;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
        User testUser = getTestUser();
        JwtResponse jwtResponse = new JwtResponse("thisisaccesstokenfortestuser");

        when(mockAuthenticationService.attemptLogin(testUser.getPrincipal().getEmail(),
                testUser.getPrincipal().getPassword()))
                .thenReturn(jwtResponse);
    }

    @Test
    public void testAuthenticationControllerInjection() {
        assertThat(authenticationController)
                .isNotNull();
    }

    @Test
    public void signIn() throws Exception {
        Principal testPrincipal = getTestUser().getPrincipal();
        JwtResponse jwtResponse = new JwtResponse("thisisaccesstokenfortestuser");
        SignInDto signInDto = new SignInDto(testPrincipal.getEmail(),
                testPrincipal.getPassword());
        MvcResult mvcResult = mockMvc.perform(post("/authentication/sign-in")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signInDto)))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
        String actual = mvcResult.getResponse().getContentAsString();
        String expected = objectMapper.writeValueAsString(jwtResponse);
        assertThat(expected)
                .isEqualToIgnoringWhitespace(actual);
    }

    @Test
    public void signUp() throws Exception {
        User testUser = getTestUser();
        doAnswer(invocationOnMock -> {
            User user = invocationOnMock.getArgument(0);
            assertThat(user).hasNoNullFieldsOrPropertiesExcept("id");
            return null;
        }).when(mockUserService).save(testUser);
        SignUpDto signUpDto = new SignUpDto(testUser.getFirstName(),
                testUser.getLastName(),
                testUser.getPrincipal().getEmail(),
                testUser.getPrincipal().getPassword());
        mockMvc.perform(post("/authentication/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(signUpDto)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void confirmEmail() throws Exception {
        String email = "example@email.com";
        String code = "this-is-test-code";
        EmailVerificationDto dto = new EmailVerificationDto();
        dto.setEmail(email);
        dto.setCode(code);
        doAnswer(invocationOnMock -> {
            List<String> expected = Arrays.asList(email, code);
            List<String> actual = Arrays.asList(invocationOnMock.getArgument(0), invocationOnMock.getArgument(1));
            assertThat(actual).isEqualTo(expected);
            return null;
        }).when(mockAuthenticationService).verifyEmail(email, code);
        mockMvc.perform(post("/authentication/confirm-email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void sendVerificationCode() throws Exception {
        String email = "example@email.com";
        SendVerificationCodeDto dto = new SendVerificationCodeDto(email);
        doAnswer(invocationOnMock -> {
            String actual = invocationOnMock.getArgument(0);
            assertThat(actual).isEqualTo(email);
            return null;
        }).when(mockAuthenticationService).sendEmailVerificationCode(email);
        mockMvc.perform(post("/authentication/send-verification-code")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void changePassword() throws Exception {
        String email = "example@email.com";
        String password = "123456Ss";
        String code = UUID.randomUUID().toString();
        ChangePasswordDto dto = new ChangePasswordDto(email, password, code);
        doAnswer(invocationOnMock -> {
            List<String> expected = Arrays.asList(email, password, code);
            List<String> actual = Arrays.asList(invocationOnMock.getArgument(0),
                    invocationOnMock.getArgument(1), invocationOnMock.getArgument(2));
            assertThat(actual).isEqualTo(expected);
            return null;
        }).when(mockAuthenticationService).changePassword(email, password, code);
        mockMvc.perform(post("/authentication/change-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    private User getTestUser() {
        Principal principal = Principal.builder()
                .password("sadsadsadsad")
                .email("sssdad@i.ua")
                .build();
        return User.builder()
                .firstName("John")
                .lastName("Cena")
                .principal(principal)
                .build();
    }
}