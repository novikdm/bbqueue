package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Principal;
import com.epam.bbqueue.entity.Role;
import com.epam.bbqueue.entity.enums.RoleName;
import com.epam.bbqueue.repository.PrincipalRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PrincipalControllerTest {
    @Autowired
    private PrincipalController principalController;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    @MockBean
    private PrincipalRepository mockPrincipalRepository;

    @Before
    public void onSetUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .build();
        Principal principal = getTestPrincipal();

        when(mockPrincipalRepository.findById(1L))
                .thenReturn(Optional.of(principal));
        when(mockPrincipalRepository.findByEmail("ssad@i.ua"))
                .thenReturn(Optional.of(principal));

        Authentication auth = new UsernamePasswordAuthenticationToken(principal, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    @Test
    public void testPrincipalControllerInjection() {
        assertThat(principalController)
                .isNotNull();
    }

    @Test
    public void getPrincipalById() throws Exception {
        Principal testPrincipal = getTestPrincipal();
        MvcResult mvcResult = mockMvc.perform(get("/principal/{principalId}", testPrincipal.getId()))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
        String actual = mvcResult.getResponse().getContentAsString();
        String expected = objectMapper.writeValueAsString(testPrincipal);
        assertThat(expected)
                .isEqualToIgnoringWhitespace(actual);
    }

    @Test
    public void getAuthenticatedPrincipal() throws Exception {
        Principal testPrincipal = getTestPrincipal();
        MvcResult mvcResult = mockMvc.perform(get("/principal-authenticated"))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
        String actual = mvcResult.getResponse().getContentAsString();
        String expected = objectMapper.writeValueAsString(testPrincipal);
        assertThat(expected)
                .isEqualToIgnoringWhitespace(actual);
    }

    private Principal getTestPrincipal() {
        Role role = new Role();
        role.setId(1L);
        role.setName(RoleName.USER);
        return Principal.builder()
                .id(1L)
                .password("sadsadsadsad")
                .email("ssad@i.ua")
                .roles(new HashSet<>(Collections.singletonList(role)))
                .build();
    }
}