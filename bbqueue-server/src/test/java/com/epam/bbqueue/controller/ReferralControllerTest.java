package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.Referral;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {ReferralController.class, GlobalExceptionHandler.class})
public class ReferralControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReferralController referralController;

    @Before
    public void initQueueController(){
        when(referralController.getReferralForUser(1L))
                .thenReturn(Arrays.asList(createReferral()));
    }

    public Referral createReferral(){
        Referral referral = new Referral();
        referral.setId(1L);
        referral.setDescription("referral");
        return referral;
    }

    @Test
    public void testGetReferralForUser() throws Exception {
        String expected = "[{\"id\":1,\"description\":\"referral\",\"doctorFrom\":null," +
                "\"doctorTo\":null,\"customer\":null,\"expirationDate\":null}]";
        mockMvc
                .perform(get("/user/referral/{id}" , 1))
                .andExpect(status().isOk())
                .andExpect(content().string(expected))
                .andDo(print());
    }
}
