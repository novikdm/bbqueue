package com.epam.bbqueue.controller;

import com.epam.bbqueue.exception.DoctorWorkDaysNotFoundException;
import com.epam.bbqueue.service.implementation.DoctorWorkDayServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {CalendarController.class, GlobalExceptionHandler.class})
public class CalendarControllerTest {

  @Autowired
  private MockMvc mockMvc;


  @MockBean
  private DoctorWorkDayServiceImpl workDayService;

  private CalendarController calendarController;

  @Before
  public void initCalendarController() {
    calendarController = new CalendarController(workDayService);
    when(workDayService.getAllDoctorWorkDays(anyLong())).thenReturn(Arrays.asList(1, 2, 3, 4));
  }

  @Test
  public void testGetDoctorWorkDaysWithGetRequestAndIllegalRequestParam() throws Exception {
    mockMvc
            .perform(get("/calendar/illegalRequestParam!"))
            .andExpect(status().isBadRequest())
            .andDo(print());
  }

  @Test
  public void testGetDoctorWorkDaysWithGetRequestAndRandomId() throws Exception {
    String expectedResponseOfWorkDays = "[1,2,3,4]";
    mockMvc
            .perform(get("/calendar/" + (long)(Math.random() * 1000000000)))
            .andExpect(status().isOk())
            .andExpect(content().string(expectedResponseOfWorkDays))
            .andDo(print());
  }

  @Test
  public void testGetDoctorWorkDaysWithStaticId() {
    assertEquals(Arrays.asList(1, 2, 3, 4), calendarController.getDoctorWorkDays("1"));
  }

  @Test
  public void testGetDoctorWorkDaysWithRandomId() {
    assertEquals(Arrays.asList(1, 2, 3, 4), calendarController.getDoctorWorkDays("" + (long)(Math.random() * 1000000000)));
  }

  @Test(expected = DoctorWorkDaysNotFoundException.class)
  public void testGetDoctorWorkDaysWithNull() {
    calendarController.getDoctorWorkDays(null);
  }
}
