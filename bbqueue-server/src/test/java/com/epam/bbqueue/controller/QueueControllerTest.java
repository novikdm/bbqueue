package com.epam.bbqueue.controller;

import com.epam.bbqueue.entity.DoctorService;
import com.epam.bbqueue.entity.TimeSlot;
import com.epam.bbqueue.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SuppressWarnings("deprecation")
@RunWith(SpringRunner.class)
@WebMvcTest(secure = false, value = {QueueController.class, GlobalExceptionHandler.class})
public class QueueControllerTest {

    @MockBean
    private QueueController queueController;

    @Autowired
    private MockMvc mockMvc;

    private TimeSlot timeSlotForTestingPostMethod;

    @Before
    public void initQueueController() throws ParseException {
        when(queueController.getListOfAllNumbersOfTimeSlot("2019-09-09", 1L))
                .thenReturn(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30));
        when(queueController.getNumberOfFreeTimeSlots("2019-09-09", 1L))
                .thenReturn(Arrays.asList(6, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30));
        timeSlotForTestingPostMethod = createTimeSlot();
        when(queueController.postTimeSlot(timeSlotForTestingPostMethod)).thenReturn(timeSlotForTestingPostMethod);
    }

    public TimeSlot createTimeSlot() {
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setVisitDate(new Date());
        timeSlot.setCustomer(new User());
        timeSlot.setTimeSlotNumber(6);
        DoctorService doctorService = new DoctorService();
        doctorService.setTimeSlotAmount(1);
        timeSlot.setService(doctorService);
        return timeSlot;
    }

    @Test
    public void testGetNumberOfAllTimeSlots() throws Exception {
        String expectedResponseOfTimeSlots = "[1,2,3,4,5,6,7,8,9,10,11,12,19,20,21,22,23,24,25,26,27,28,29,30]";
        mockMvc
                .perform(get("/queue/all/2019-09-09-1"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponseOfTimeSlots))
                .andDo(print());
    }

    @Test
    public void testGetNumberOfFreeTimeSlots() throws Exception {
        String expectedResponseOfTimeSlots = "[6,21,22,23,24,25,26,27,28,29,30]";
        mockMvc
                .perform(get("/queue/free/2019-09-09-1"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedResponseOfTimeSlots))
                .andDo(print());
    }

    @Test
    public void testPostTimeSlot() throws Exception {
        mockMvc
                .perform(post("/queue")
                        .content(asJsonString(timeSlotForTestingPostMethod))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.timeSlotNumber")
                        .value(timeSlotForTestingPostMethod.getTimeSlotNumber()))
                .andExpect(jsonPath("$.customer")
                        .value(timeSlotForTestingPostMethod.getCustomer()))
                .andExpect(jsonPath("$.service")
                        .value(timeSlotForTestingPostMethod.getService()))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testPostTimeSlot_Should_Fail() throws Exception {
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setTimeSlotNumber(0);
        timeSlot.setService(new DoctorService());
        mockMvc.perform(post("/queue")
                .content(asJsonString(timeSlot))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").isArray())
                .andExpect(jsonPath("$.errors", hasSize(3)))
                .andExpect(jsonPath("$.errors", hasItem("TimeSlot number must be greater than 0")))
                .andExpect(jsonPath("$.errors", hasItem("User should not be null")))
                .andExpect(jsonPath("$.errors", hasItem("Date should not be null")));

    }

    private String asJsonString(final TimeSlot timeSlot) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(timeSlot);
    }
}