import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DatepickerComponent } from '../../components/datepicker/datepicker.component';

@NgModule({
  imports: [BrowserModule, FormsModule, NgbModule],
  declarations: [DatepickerComponent],
  exports: [DatepickerComponent],
  bootstrap: [DatepickerComponent]
})
export class DatepickerModule {}
