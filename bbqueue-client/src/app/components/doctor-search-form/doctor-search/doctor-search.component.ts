import {Component, Input, OnInit} from '@angular/core';
import {Hospital} from '../../../models/entity/hospital';
import {DoctorSearchService} from '../../../services/doctor/doctor-search.service';
import {DoctorSearchFormDto} from '../../../models/dto/doctor-search-form-dto';
import {DoctorSpeciality} from '../../../models/entity/doctor-speciality';

@Component({
  selector: 'app-doctor-search',
  templateUrl: './doctor-search.component.html',
  styleUrls: ['./doctor-search.component.css']
})
export class DoctorSearchComponent implements OnInit {

  @Input() hospital: Hospital;
  @Input() doctorSpeciality: DoctorSpeciality;
  @Input() doctorsInHospital: DoctorSearchFormDto[];
  @Input() doctorsWithSpeciality: DoctorSearchFormDto[];
  @Input() doctorLastNameWithSpeciality: string[];
  @Input() savedDoctorLastNameWithSpeciality: string[];
  private doctorsWithNameNoSpeciality: DoctorSearchFormDto[];
  private doctorsWithNameWithSpeciality: DoctorSearchFormDto[];
  private doctorLastNameNoSpeciality: string[];
  private savedDoctorLastNameNoSpeciality: string[];
  private doctorNameNoSpeciality: string;
  private doctorNameWithSpeciality: string;

  constructor(
    private doctorSearchService: DoctorSearchService,
  ) {
  }

  ngOnInit() {
    this.doctorLastNameNoSpeciality = this.doctorsInHospital.map(value => value.lastName);
    this.savedDoctorLastNameNoSpeciality = this.doctorsInHospital.map(value => value.lastName);
  }

  onClickToGetDoctorNames(doctorName) {
    if (this.savedDoctorLastNameNoSpeciality.map(v => v.toLowerCase())
      .includes(this.doctorNameNoSpeciality.toLowerCase())) {
      this.doctorSearchService.getDoctorsByName(this.hospital.id, doctorName).subscribe(doctor => {
        this.doctorsWithNameNoSpeciality = doctor;
      });
    }
  }

  onClickToGetDoctorNamesAndSpeciality(doctorName) {
    if (this.savedDoctorLastNameWithSpeciality.map(v => v.toLowerCase())
      .includes(this.doctorNameWithSpeciality.toLowerCase())) {
      this.doctorSearchService
        .getDoctorsBySpecialityAndLastName(this.doctorSpeciality.id, this.hospital.id, doctorName)
        .subscribe(doctor => {
          this.doctorsWithNameWithSpeciality = doctor;
        });
    }
  }

  checkingDoctorsForOutput() {
    if (this.doctorsWithNameWithSpeciality !== undefined) {
      return this.doctorsWithNameWithSpeciality.sort((a, b) => a.lastName.localeCompare(b.lastName));
    } else if (this.doctorsWithSpeciality !== undefined) {
      return this.doctorsWithSpeciality.sort((a, b) => a.lastName.localeCompare(b.lastName));
    } else if (this.doctorsWithNameNoSpeciality !== undefined) {
      return this.doctorsWithNameNoSpeciality.sort((a, b) => a.lastName.localeCompare(b.lastName));
    } else {
      return this.doctorsInHospital.sort((a, b) => a.lastName.localeCompare(b.lastName));
    }
  }

  clickBackSpaceForDoctorNameNoSpeciality() {
    this.doctorsWithNameNoSpeciality = undefined;
  }

  clickBackSpaceForDoctorNameWithSpeciality() {
    this.doctorsWithNameWithSpeciality = undefined;
  }

  inputToGetLastName() {
    this.doctorLastNameNoSpeciality = this.savedDoctorLastNameNoSpeciality
      .filter(value => value.toLowerCase().includes(this.doctorNameNoSpeciality.toLowerCase()));
  }

  inputToGetLastNameWithSpeciality() {
    this.doctorLastNameWithSpeciality = this.savedDoctorLastNameWithSpeciality
      .filter(value => value.toLowerCase().includes(this.doctorNameWithSpeciality.toLowerCase()));
  }

  clickToSelectNameNoSpeciality(e) {
    this.onClickToGetDoctorNames(e.target.value);
  }

  clickToSelectNameWithSpeciality(e) {
    this.onClickToGetDoctorNamesAndSpeciality(e.target.value);
  }
}
