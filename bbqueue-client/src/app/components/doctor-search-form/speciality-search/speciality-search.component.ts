import {Component, Input, OnInit} from '@angular/core';
import {Hospital} from '../../../models/entity/hospital';
import {DoctorSpeciality} from '../../../models/entity/doctor-speciality';
import {SpecialitySearchService} from '../../../services/speciality/speciality-search.service';
import {DoctorSearchService} from '../../../services/doctor/doctor-search.service';
import {DoctorSearchFormDto} from '../../../models/dto/doctor-search-form-dto';

@Component({
  selector: 'app-speciality-search',
  templateUrl: './speciality-search.component.html',
  styleUrls: ['./speciality-search.component.css']
})
export class SpecialitySearchComponent implements OnInit {

  @Input() hospital: Hospital;
  @Input() doctorsInHospital: DoctorSearchFormDto[];
  private savedSpecialitiesInHospital: string[];
  private specialitiesInHospital: string[];
  private specialityName: string;
  private doctorSpeciality: DoctorSpeciality;
  private doctorsWithSpeciality: DoctorSearchFormDto[];
  private doctorLastNameWithSpeciality: string[];
  private savedDoctorLastNameWithSpeciality: string[];

  constructor(
    private specialitySearchService: SpecialitySearchService,
    private doctorSearchService: DoctorSearchService
  ) {
  }

  ngOnInit() {
    this.savedSpecialitiesInHospital = this.doctorsInHospital.map(value => value.speciality)
      .filter((value, index, self) => self.indexOf(value) === index);
    this.specialitiesInHospital = this.doctorsInHospital.map(value => value.speciality)
      .filter((value, index, self) => self.indexOf(value) === index);
  }

  onClickToGetSpeciality(specialityName) {
    if (this.savedSpecialitiesInHospital.map(value => value.toLowerCase()).includes(this.specialityName.toLowerCase())) {
      this.specialitySearchService.getDoctorSpecialityByName(specialityName).subscribe(speciality => {
        this.doctorSpeciality = speciality;
        this.onClickToGetDoctorsWithSpeciality(specialityName);
      });
    }
  }

  onClickToGetDoctorsWithSpeciality(specialityName) {
    if (this.doctorSpeciality !== undefined) {
      this.doctorSearchService.getDoctorsBySpeciality
      (specialityName, this.hospital.id).subscribe(doctors => {
        this.doctorsWithSpeciality = doctors;
        this.doctorLastNameWithSpeciality = this.doctorsWithSpeciality.map(value => value.lastName);
        this.savedDoctorLastNameWithSpeciality = this.doctorsWithSpeciality.map(value => value.lastName);
      });
    }
  }

  clickBackSpace() {
    this.doctorSpeciality = null;
    this.doctorsWithSpeciality = undefined;
    this.doctorLastNameWithSpeciality = undefined;
  }

  inputToGetSpeciality() {
    this.specialitiesInHospital = this.savedSpecialitiesInHospital
      .filter(value => value.toLowerCase().includes(this.specialityName.toLowerCase()));
    if (this.specialitiesInHospital !== undefined) {
      this.doctorSpeciality = null;
    }
  }

  clickToSelectSpeciality(e) {
    this.onClickToGetSpeciality(e.target.value);
  }
}
