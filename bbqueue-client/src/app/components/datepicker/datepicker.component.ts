import {Component, Input, Output} from '@angular/core';
import {NgbDatepickerConfig, NgbCalendar, NgbDate, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {getToday} from "@progress/kendo-angular-dateinputs/dist/es2015/util";
import {CalendarService} from '../../services/calendar/calendar.service';


@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styles: [`
    .custom-day {
        color: rgba( 50, 200, 100, 0.7);
    }
    .custom-day:hover {
        border-radius: 2rem;
        background-color: rgba( 50, 150, 255, 0.1);
        /*background-color: #007bff;*/
    }
    .disabled:hover {
        border-radius: 2rem;
        background-color: rgba( 255, 255, 255, 1);
    }
    .weekend:hover  {
        border-radius: 2rem;
        background-color: rgba( 255, 255, 255, 1);
    }
    .weekend {
        color: rgba( 245, 120, 120, 0.85) !important;
    }
    .disabled {
        color: rgba( 100, 100, 100, 0.6) !important;
    }
    .hidden {
      display: none;
    }
    .selected {
        border-color: rgba( 50, 255, 120, 0.3);
        border-radius: 2rem;
        background-color: rgba( 50, 255, 120, 0.3);
        color: rgba( 50, 200, 50, 1);
    }
  `],
  providers: [NgbDatepickerConfig]
})
export class DatepickerComponent {

  model: NgbDateStruct;
  workDayList: number[];
  @Input("doctorIdInDatepicker") doctorID: number;
  selectedDate: NgbDate;

  constructor(private config: NgbDatepickerConfig,
              private calendar: NgbCalendar,
              private calendarService: CalendarService) {
  }

  ngOnInit() {
    this.calendarService.onClick.subscribe(doctorId => this.doctorID = doctorId);
    console.log(this.doctorID);
    this.calendarService.findDoctorWorkingDaysByDoctorId(this.doctorID).subscribe(
      data => this.workDayList = data
    );
    this.config.minDate = {year: 2019, month: 1, day: 1};
    this.config.maxDate = {year: 2099, month: 12, day: 31};
    this.config.markDisabled = (date: NgbDate) =>
      !this.workDayList.includes(this.calendar.getWeekday(date)) || this.isDisabled(date);
  }

  isDisabled = (date: NgbDate) =>
    ((date.month < (getToday().getMonth() + 1)) || (date.month > (getToday().getMonth() + 2))) ||
    ((date.month === (getToday().getMonth() + 1)) && (date.day < getToday().getDate()));

  isWeekend = (date: NgbDate) =>  !this.workDayList.includes(this.calendar.getWeekday(date));

  setSelectedDate = (date: NgbDate) => {
    confirm("Do you want select this date - " + this.getDateString(date) + "?") ?
    this.selectedDate = date : alert("Select date!");
  };

  private getDateString = (date: NgbDate) => {
    return "" + date.year + "-" +date.month + "-" + date.day + "-" + this.doctorID;
  };
}
