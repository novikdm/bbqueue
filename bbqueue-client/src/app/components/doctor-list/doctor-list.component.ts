import {Component, Input, OnInit} from '@angular/core';
import {DoctorSearchFormDto} from '../../models/dto/doctor-search-form-dto';
import {CalendarService} from '../../services/calendar/calendar.service';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent implements OnInit {
  @Input('doctorsList') doctors: DoctorSearchFormDto[];
  localDoctorId : number;

  constructor(private calendarService: CalendarService) {
    this.calendarService.onClick.subscribe(doctorId => this.localDoctorId = doctorId);
  }

  public getCalendarByDoctorId(doctorId : number) {
    this.localDoctorId = doctorId;
    this.calendarService.openCalendarByDoctorId(doctorId);
  }

  ngOnInit() {
  }

}
