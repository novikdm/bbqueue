import {Component, OnInit} from '@angular/core';
import {SignUpDto} from '../../../models/dto/sign-up-dto';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalService} from '../../../services/modal.service';
import {SignInDto} from '../../../models/dto/sign-in-dto';
import {MustMatch} from '../../../helpers/must-match.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  form: FormGroup;
  notVerified = false;
  verificationEmail: string;
  errorMessage: string;
  isLoading = false;
  isPasswordShown = false;
  isConfirmPasswordShown = false;

  constructor(
    private authenticationService: AuthenticationService,
    private modalService: ModalService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
    firstName: new FormControl('', [Validators.required,
      Validators.pattern('(^[A-Za-z]+$)|(^[А-ЯЄІЇа-яєії]+$)')]),
    lastName: new FormControl('', [Validators.required,
      Validators.pattern('(^[A-Za-z]+$)|(^[А-ЯЄІЇа-яєії]+$)')]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required,
      Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{3,}'),
      Validators.minLength(8)]),
    confirmPassword: new FormControl('', [Validators.required])
  }, {validator: MustMatch('password', 'confirmPassword')});
  }

  ngOnInit() {
  }

  signUp() {
    if (this.form.invalid) {
      Object.keys(this.form.controls)
        .forEach(controlName => this.form.controls[controlName].markAsTouched());
      return;
    }
    this.isLoading = true;
    const signUpDto = new SignUpDto();
    signUpDto.firstName = this.form.controls.firstName.value;
    signUpDto.lastName = this.form.controls.lastName.value;
    signUpDto.email = this.form.controls.email.value;
    signUpDto.password = this.form.controls.password.value;
    this.authenticationService.signUp(signUpDto)
      .subscribe(() => {
        this.isLoading = false;
        this.verificationEmail = signUpDto.email;
        this.notVerified = true;
      }, (errorResponse) => {
        this.isLoading = false;
        if (errorResponse.status === 409) {
          this.form.controls.email.setErrors({occupied: true});
        } else {
          this.errorMessage = errorResponse.error;
        }
      });
  }

  onVerification(success: boolean) {
    if (success) {
      this.notVerified = false;
      this.isLoading = true;
      const signInForm = new SignInDto();
      signInForm.password = this.form.controls.password.value;
      signInForm.email = this.form.controls.email.value;
      this.authenticationService.signIn(signInForm).subscribe(() => {
          this.isLoading = false;
          this.modalService.hideAuthentication();
        },
        () => this.isLoading = false);
    }
  }

  hasError(controlName: string, errorCode: string): boolean {
    return this.form.controls[controlName].touched && this.form.controls[controlName].hasError(errorCode);
  }
}
