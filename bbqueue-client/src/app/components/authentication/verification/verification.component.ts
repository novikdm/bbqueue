import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModalService} from '../../../services/modal.service';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {EmailVerificationDto} from '../../../models/dto/email-verification-dto';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit {
  form: FormGroup;
  @Input()
  email: string;
  @Output()
  closeEvent = new EventEmitter<boolean>();
  isCodeSending = false;
  errorMessage: string;
  isLoading = false;

  constructor(
    private modalService: ModalService,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      code: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9-]+')])
    });
  }

  ngOnInit() {
    this.sendCode();
  }

  private sendCode() {
    this.isCodeSending = true;
    const dto = new EmailVerificationDto(this.email);
    this.authService.sendVerificationCode(dto).subscribe(() => this.isCodeSending = false);
  }

  verifyEmail() {
    if (this.form.invalid) {
      Object.keys(this.form.controls)
        .forEach(controlName => this.form.controls[controlName].markAsTouched());
      return;
    }
    this.isLoading = true;
    this.errorMessage = null;
    const dto = new EmailVerificationDto(this.email, this.form.controls.code.value);
    this.authService.verifyEmail(dto)
      .subscribe(() => {
        this.closeEvent.emit(true);
        this.isLoading = false;
      }, (errorResponse) => {
        this.isLoading = false;
        if (errorResponse.status === 422) {
          this.form.controls.code.setErrors({wrongCode: true});
        } else {
          this.errorMessage = errorResponse.error;
        }
      });
  }

  censorWord(str: string) {
    const starsNumber = str.length - 2 > 4 ? 4 : str.length - 2;
    return str[0] + '*'.repeat(starsNumber) + str.slice(-1);
  }

  censorEmail(email) {
    const arr = email.split('@');
    return this.censorWord(arr[0]) + '@' + this.censorWord(arr[1]);
  }

  hasError(controlName: string, errorCode: string): boolean {
    return this.form.controls[controlName].touched && this.form.controls[controlName].hasError(errorCode);
  }
}
