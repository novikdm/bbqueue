import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication/authentication.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MustMatch} from '../../../helpers/must-match.validator';
import {SendVerificationCodeDto} from '../../../models/dto/send-verification-code-dto';
import {ChangePasswordDto} from '../../../models/dto/change-password-dto';
import {Alert} from '../../../models/view/alert';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.css']
})
export class PasswordRecoveryComponent implements OnInit {
  @Input()
  email: string;
  @Output()
  closeEvent = new EventEmitter<boolean>();
  alert: Alert;
  isCodeSending = false;
  isCodeSent = false;
  sendCodeForm: FormGroup;
  changePasswordForm: FormGroup;
  isPasswordShown = false;
  isConfirmPasswordShown = false;
  changingPassword = false;

  constructor(
    private authService: AuthenticationService,
    private formBuilder: FormBuilder
  ) {
    this.sendCodeForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email])
    });
    this.changePasswordForm = this.formBuilder.group({
      code: new FormControl('', [Validators.required, Validators.pattern('[a-z0-9-]+')]),
      password: new FormControl('', [Validators.required,
        Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{3,}'),
        Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required])
    }, {validator: MustMatch('password', 'confirmPassword')});
  }

  ngOnInit() {
    this.sendCodeForm.controls.email.setValue(this.email);
  }

  hasError(control: AbstractControl, errorCode: string): boolean {
    return control.touched && control.hasError(errorCode);
  }

  sendCode() {
    Object.keys(this.sendCodeForm.controls)
      .forEach(controlName => this.sendCodeForm.controls[controlName].markAsTouched());
    if (this.sendCodeForm.invalid) {
      return;
    }
    this.isCodeSending = true;
    const email = this.sendCodeForm.controls.email.value;
    this.email = email;
    const dto = new SendVerificationCodeDto(email);
    this.authService.sendVerificationCode(dto)
      .subscribe(() => {
        this.isCodeSending = false;
        this.sendCodeForm.disable();
        this.isCodeSent = true;
      }, errorResponse => {
        this.isCodeSending = false;
        if (errorResponse.status === 404) {
          this.sendCodeForm.controls.email.setErrors({notExists: true});
        }
      });
  }

  changePassword() {
    if (this.changePasswordForm.invalid) {
      Object.keys(this.sendCodeForm.controls)
        .forEach(controlName => this.sendCodeForm.controls[controlName].markAsTouched());
      return;
    }
    this.changingPassword = true;
    const dto = new ChangePasswordDto();
    dto.email = this.email;
    dto.password = this.changePasswordForm.controls.password.value;
    dto.code = this.changePasswordForm.controls.code.value;
    this.authService.changePassword(dto)
      .subscribe(() => {
        this.changingPassword = false;
        this.closeEvent.emit(true);
      }, errorResponse => {
        if (errorResponse.status === 422) {
          this.changePasswordForm.controls.code.setErrors({wrongCode: true});
        } else {
          this.changingPassword = false;
          this.alert = Alert.danger(errorResponse.statusText);
          this.closeEvent.emit(false);
        }
      });
  }

  censorWord(str: string) {
    const starsNumber = str.length - 2 > 4 ? 4 : str.length - 2;
    return str[0] + '*'.repeat(starsNumber) + str.slice(-1);
  }

  censorEmail(email: string) {
    const arr = email.split('@');
    return this.censorWord(arr[0]) + '@' + this.censorWord(arr[1]);
  }
}
