import { Component, OnInit } from '@angular/core';
import { Referral } from 'src/app/models/entity/referral';
import { ReferralService } from 'src/app/services/referral.service'
@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit {

  referrals: Referral[];

  constructor(
    private referralService: ReferralService
  ) {

   }

  ngOnInit() {
    this.referralService.getReferralForUser(5).subscribe(data => {
      this.referrals = data;
    });
  }

}
