import { Component, OnInit } from '@angular/core';
import { TimeSlot } from 'src/app/models/entity/time-slot';
import { VisitResultService } from 'src/app/services/visit-result.service';

@Component({
  selector: 'app-history-visit',
  templateUrl: './history-visit.component.html',
  styleUrls: ['./history-visit.component.css']
})
export class HistoryVisitComponent implements OnInit {

  timeSlots: TimeSlot[];

  constructor(
    private visitResultService: VisitResultService
  ) { }

  ngOnInit() {
    this.visitResultService.getAllHistoryOfVisitsForUser(1).subscribe(data => {
      this.timeSlots = data;
    });
  }
}
