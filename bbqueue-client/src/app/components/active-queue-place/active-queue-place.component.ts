import { Component, OnInit } from '@angular/core';
import { TimeSlot } from 'src/app/models/entity/time-slot';
import { VisitResultService } from 'src/app/services/visit-result.service';

@Component({
  selector: 'app-active-queue-place',
  templateUrl: './active-queue-place.component.html',
  styleUrls: ['./active-queue-place.component.css']
})
export class ActiveQueuePlaceComponent implements OnInit {

  timeSlots: TimeSlot[];

  constructor(
    private visitResultService: VisitResultService
  ) { }

  ngOnInit() {
    this.visitResultService.getAllActiveQueuePlace(1).subscribe(data => {
      this.timeSlots = data;
    });
  }

}
