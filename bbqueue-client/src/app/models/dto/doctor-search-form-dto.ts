export class DoctorSearchFormDto {
  constructor(
    public doctorId: number,
    public cabinetNumber: number,
    public firstName: string,
    public lastName: string,
    public speciality: string,
    public hospital: string
  ) {}
}
