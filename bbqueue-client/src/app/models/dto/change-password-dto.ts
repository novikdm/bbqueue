export class ChangePasswordDto {
  constructor(
    public email?: string,
    public password?: string,
    public code?: string,
  ) {
  }
}
