export class EmailVerificationDto {
  constructor(
    public email?: string,
    public code?: string,
  ) {
  }
}
