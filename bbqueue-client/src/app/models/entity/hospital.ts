import {Region} from './region';
import {Doctor} from './doctor';

export class Hospital {
  constructor(
    public id: number,
    public name: string,
    public region: Region,
    public headDoctor: Doctor
  ) {}
}
