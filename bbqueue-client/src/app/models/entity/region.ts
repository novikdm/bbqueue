import {Country} from './country';

export class Region {
  constructor(
    public id: number,
    public name: string,
    public country?: Country
  ) {
  }
}

