import {Principal} from './principal';

export class User {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public principal?: Principal
  ) {
  }
}
