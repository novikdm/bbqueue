import { Doctor } from './doctor';
import { User } from './user';

export class Referral {
    constructor(
        public id: number,
        public description: string,
        public doctorFrom: Doctor,
        public doctorTo: Doctor,
        public expirationDate: Date,
        public user?: User
    ){
    }
}
