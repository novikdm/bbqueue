import {Doctor} from './doctor';

export class DoctorSpeciality{
  constructor(
    public id: number,
    public name: string,
  ) {}
}
