import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Referral } from 'src/app/models/entity/referral';
import {ApiUrls} from 'src/app/const/api-urls';


@Injectable({
  providedIn: 'root'
})
export class ReferralService {

  constructor(
    private http: HttpClient
  ) {  
  }

public getReferralForUser(id: number): Observable<Referral[]>{
  return this.http.get<Referral[]>(ApiUrls.REFERRAL_GET_FOR_USER + id)
}

}
