import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ApiUrls} from '../../const/api-urls';
import {Region} from '../../models/entity/region';

@Injectable({
  providedIn: 'root'
})
export class RegionSearchService {
  constructor(
    private httpClient: HttpClient
  ) {
  }

  public getRegionByName(name: string): Observable<Region> {
    return this.httpClient.get<Region>(ApiUrls.REGION_GET_BY_NAME + name);
  }

  public getAllRegionNames(): Observable<string[]> {
    return this.httpClient.get<string[]>(ApiUrls.REGION_GET_ALL_NAMES);
  }
}
