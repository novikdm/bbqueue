import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ApiUrls} from '../../const/api-urls';
import {Observable} from 'rxjs';
import {Hospital} from '../../models/entity/hospital';
import {DoctorSearchFormDto} from '../../models/dto/doctor-search-form-dto';

@Injectable({
  providedIn: 'root'
})
export class HospitalSearchService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  public getHospitalByNameAndRegion(name: string, id: number): Observable<Hospital> {
    const params = new HttpParams()
      .set('hospitalName', name)
      .set('regionId', String(id));
    return this.httpClient.get<Hospital>(ApiUrls.HOSPITAL_GET_BY_NAME_AND_REGION, {params});
  }

  public getAllHospitalNames(regionId: number): Observable<string[]> {
    return this.httpClient.get<string[]>(ApiUrls.HOSPITAL_GET_ALL_NAMES + regionId);
  }

  public getDoctorsByHospital(name: string): Observable<DoctorSearchFormDto[]> {
    return this.httpClient.get<DoctorSearchFormDto[]>(ApiUrls.DOCTORS_GET_BY_HOSPITAL_NAME + name);
  }
}
