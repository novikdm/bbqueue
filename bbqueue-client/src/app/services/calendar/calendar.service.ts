import {EventEmitter, Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';
import {ApiUrls} from '../../const/api-urls';

@Injectable()
export class CalendarService {

  doctorId : number;
  onClick:EventEmitter<number> = new EventEmitter();

  constructor(private http: HttpClient) {
  }

  public findDoctorWorkingDaysByDoctorId(doctorId : number) : Observable<number[]> {
    return this.http.get<number[]>(ApiUrls.CALENDAR_GET_BY_DOCTOR_ID  + this.doctorId);
  }

  public openCalendarByDoctorId(doctorId : number) {
    this.doctorId = doctorId;
    this.onClick.emit(this.doctorId);
  }
}
