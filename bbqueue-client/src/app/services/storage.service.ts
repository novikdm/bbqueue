import {Injectable} from '@angular/core';

const TOKEN_KEY = 'BBQueueAuthenticationToken';
const EMAIL_KEY = 'BBQueueAuthenticationEmail';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() {
  }

  static clear() {
    window.localStorage.clear();
  }

  public static set token(token: string) {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public static get token(): string {
    return localStorage.getItem(TOKEN_KEY);
  }

  public static set email(email: string) {
    window.localStorage.removeItem(EMAIL_KEY);
    window.localStorage.setItem(EMAIL_KEY, email);
  }

  public static get email(): string {
    return localStorage.getItem(EMAIL_KEY);
  }
}
