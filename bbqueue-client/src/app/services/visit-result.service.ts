import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TimeSlot } from '../models/entity/time-slot';
import { ApiUrls } from 'src/app/const/api-urls'

@Injectable({
  providedIn: 'root'
})
export class VisitResultService {

  constructor(
    private http: HttpClient
  ) { }

  public getAllHistoryOfVisitsForUser(id: number): Observable<TimeSlot[]>{
    return this.http.get<TimeSlot[]>(ApiUrls.HISTORY_GET_ALL_VISITS_FOR_USER + id)
  }

  public getAllActiveQueuePlace(id: number): Observable<TimeSlot[]>{
    return this.http.get<TimeSlot[]>(ApiUrls.ACTIVE_GET_ALL_QUEUE_PLACE + id)
  }
}
