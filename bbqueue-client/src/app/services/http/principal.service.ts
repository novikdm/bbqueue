import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Principal} from '../../models/entity/principal';
import {ApiUrls} from '../../const/api-urls';

@Injectable({
  providedIn: 'root'
})
export class PrincipalService {

  constructor(
    private http: HttpClient
  ) {
  }

  public getPrincipalById(id: number): Observable<Principal> {
    return this.http.get<Principal>(ApiUrls.PRINCIPAL_GET_BY_ID + id);
  }

  public getAuthenticatedPrincipal(): Observable<Principal> {
    return this.http.get<Principal>(ApiUrls.PRINCIPAL_GET_AUTHENTICATED);
  }
}
