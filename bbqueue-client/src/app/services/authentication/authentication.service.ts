import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SignInDto} from '../../models/dto/sign-in-dto';
import {ApiUrls} from '../../const/api-urls';
import {map} from 'rxjs/operators';
import {StorageService} from '../storage.service';
import {SignUpDto} from '../../models/dto/sign-up-dto';
import {JwtResponse} from '../../models/dto/jwt-response';
import {Observable} from 'rxjs';
import {Principal} from '../../models/entity/principal';
import {PrincipalService} from '../http/principal.service';
import {EmailVerificationDto} from '../../models/dto/email-verification-dto';
import {ChangePasswordDto} from '../../models/dto/change-password-dto';
import {SendVerificationCodeDto} from '../../models/dto/send-verification-code-dto';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private signedInPrincipal: Principal;

  constructor(
    private http: HttpClient,
    private principalService: PrincipalService
  ) {
  }

  public get principal(): Principal {
    return this.signedInPrincipal;
  }

  public get isLogged(): boolean {
    return !!this.principal;
  }

  public requestSignedInPrincipal(): void {
    this.requestPrincipal();
  }

  private requestPrincipal(): void {
    if (!this.signedInPrincipal) {
      this.principalService.getAuthenticatedPrincipal()
        .subscribe(principal => {
          this.signedInPrincipal = principal;
        }, () => {
          setTimeout(() => this.requestPrincipal(),
            5000);
        });
    }
  }

  public signIn(form: SignInDto): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(ApiUrls.SIGN_IN, form)
      .pipe(map(response => {
        if (response.accessToken) {
          StorageService.token = response.accessToken;
          this.requestPrincipal();
        }
        return response;
      }));
  }

  public signUp(form: SignUpDto): Observable<boolean> {
    return this.http.post<boolean>(ApiUrls.SIGN_UP, form);
  }

  public logOut() {
    this.signedInPrincipal = null;
    StorageService.clear();
  }

  public sendVerificationCode(dto: SendVerificationCodeDto): Observable<any> {
    return this.http.post<any>(ApiUrls.SEND_VERIFICATION_CODE, dto);
  }

  public verifyEmail(dto: EmailVerificationDto): Observable<any> {
    return this.http.post<any>(ApiUrls.CONFIRM_EMAIL, dto);
  }

  public changePassword(dto: ChangePasswordDto): Observable<any> {
    return this.http.post<any>(ApiUrls.CHANGE_PASSWORD, dto);
  }
}
