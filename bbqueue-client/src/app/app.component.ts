import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './services/authentication/authentication.service';
import {StorageService} from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private authService: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    if (StorageService.token) {
      this.authService.requestSignedInPrincipal();
    }
  }
}
